resource "aws_s3_bucket" "your-fbt" {
  bucket_prefix = "your-fbt-bucket"

  tags = {
    Name        = "your-name"
    Environment = "your-name"
  }
}
